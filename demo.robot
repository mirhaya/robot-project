*** Settings ***
Suite Setup
Suite Teardown
Library           SeleniumLibrary

*** Test Cases ***
Simple Form Demo
    [Setup]    MainPage
    ModalWindowHandle
    GetToSimpleForm
    Scroll Element Into View    xpath://*[@id="easycont"]/div/div[2]/div[2]/div[1]
    Sleep    3
    Input Text    id:user-message    hello this is haya
    Sleep    3
    Click Button    xpath://*[@id="get-input"]/button
    Sleep    2
    ${message}    Get Text    xpath://*[@id="user-message"]/label
    ${new}    Get Text    xpath://*[@id="display"]
    log    ${message} ${new}
    Scroll Element Into View    xpath://*[@id="easycont"]/div/div[2]/div[2]/div[2]/div
    Sleep    2
    Input Text    id:sum1    5
    Sleep    2
    Input Text    id:sum2    6
    Sleep    2
    Click Button    xpath://*[@id="gettotal"]/button
    Sleep    2
    ${sum}    Get Text    xpath://*[@id="easycont"]/div/div[2]/div[2]/div[2]/div/label
    ${sum1}    Get Text    xpath://*[@id="displayvalue"]
    Log    ${sum}${sum1}
    [Teardown]    Close Browser
    *** Keywords ***
MainPage
    Open Browser    https://www.seleniumeasy.com/    chrome
    Maximize Browser Window
    Sleep    3
    Click Link    xpath://*[@id="block-block-57"]/div/div/a
    Sleep    2
    [Teardown]

ModalWindowHandle
    Click Link    id:at-cv-lightbox-close
    Sleep    3

GetToSimpleForm
    Scroll Element Into View    xpath://*[@id="btn_basic_example"]
    Click Link    xpath://*[@id="treemenu"]/li/ul/li[1]/a
    Sleep    2
    Click Link    xpath://*[@id="treemenu"]/li/ul/li[1]/ul/li[1]/a
    Sleep    2

GetToCheckbox
    Scroll Element Into View    xpath://*[@id="btn_basic_example"]
    Click Link    xpath://*[@id="treemenu"]/li/ul/li[1]/a
    Sleep    2
    Click Link    xpath://*[@id="treemenu"]/li/ul/li[1]/ul/li[2]/a
    Sleep    2

GetToRadioButton
    Scroll Element Into View    xpath://*[@id="btn_basic_example"]
    Click Link    xpath://*[@id="treemenu"]/li/ul/li[1]/a
    Sleep    2
    Click Link    xpath://*[@id="treemenu"]/li/ul/li[1]/ul/li[3]/a
    Sleep    2

GetToDropDown
    Scroll Element Into View    xpath://*[@id="btn_basic_example"]
    Click Link    xpath://*[@id="treemenu"]/li/ul/li[1]/a
    Sleep    2
    Click Link    xpath://*[@id="treemenu"]/li/ul/li[1]/ul/li[4]/a
    Sleep    2

GetToInputForm
    Scroll Element Into View    xpath://*[@id="btn_basic_example"]
    Click Link    xpath://*[@id="treemenu"]/li/ul/li[1]/a
    Sleep    2
